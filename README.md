# RECAST Instructions
Clone RECAST from this repo: https://gitlab.cern.ch/recast-atlas/susy/ana-susy-2018-16. These instructions essentially summarize the README there.

## Setup
First, will need to set these parameters in reana.yml to the full file paths for each file:
    
    - dxaod_file_mc16a
    - dxaod_file_mc16d
    - dxaod_file_mc16e 
    - ISR_unc_file
    - signal_xsec

**To use Reana, these files need to be stored in eos!

The "dxaod_file_mc16a/d/e" inputs are the full path to the directory containing signal DxAODs.

The ISR_unc_file refers to a json file that stores ISR uncertainties for each signal region. Looks like this:
<pre>
{
    "SR_eMLL_hghmet":0.01,
    "SR_eMLL_lowmet_deltaM_low":0.01,
    "SR_eMLL_lowmet_deltaM_high":0.01,
    "SR_eMT2_hghmet":0.01,
    "SR_eMT2_lowmet_V2":0.01
}
</pre>

The signal_xsec refers to a txt file that stores info about the "final state" (for us this is 112), cross section, branching fraction, filter effeciency, and relative uncertainty for each DSID. When running with one set of samples on one signal process, it looks like this: 

<pre>
#datasetID      finalState      XS      BF      filtEffic       relUncertainty  notes
450962  112     3.73        1       1.94E-01        5.00E-02        
</pre>

For running multiple signals through (like ggF + VBF), just need to add the paths to the DxAODS in reana.yml:
<pre>
dxaod_file_mc16a/d/e: 
    - '/path/to/first/signal/dir'
    - '/path/to/second/signal/dir'
</pre>

And also add the second DSID to signal_xsec.txt:
<pre>
#datasetID      finalState      XS      BF      filtEffic       relUncertainty  notes
DSID1  112     3.73        1       1.94E-01        5.00E-02
DSID2  112     48.52       1       1.94E-01        5.00E-02     
</pre>

Now to setup Reana. If it's the first time using it, log on to https://reana.cern.ch/. Click on the profile icon in the top right corner and generate your access token. Now on lxplus, you'll need to set up the environment like so:

<pre>
source /afs/cern.ch/user/r/reana/public/reana/bin/activate
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=XXXXXXX
</pre>

Now you'll need to set up the Kerberos authentication (replace username with your cern username):

<pre>
# Generate the keytab file
ktutil
ktutil:  add_entry -password -p username@CERN.CH -k 1 -e aes256-cts-hmac-sha1-96
Password for username@CERN.CH:
ktutil:  add_entry -password -p username@CERN.CH -k 1 -e arcfour-hmac
Password for username@CERN.CH:
ktutil:  write_kt .keytab
ktutil:  exit
# Upload the keytab file as a secret to the REANA clutster
reana-client secrets-add --env CERN_USER=username --env CERN_KEYTAB=.keytab --file .keytab
</pre>

## Running RECAST
To run with default reana.yml:
<pre>
reana-client run -w myAnalysis
</pre>
To use a different input file, just add -f:
<pre>
reana-client run -w myAnalysis -f NewInput.yml
</pre>
To check on the status of the job, you can go to the reana website, or use:
<pre>
reana-client status -w myAnalysis
</pre>
To download all the results of the fits:
<pre>
reana-client ls -w myAnalysis  | grep ^fit_MLL | awk '{print $1}' | xargs reana-client download -w myAnalysis -o myOutputDirectory
reana-client ls -w myAnalysis  | grep ^fit_MT2 | awk '{print $1}' | xargs reana-client download -w myAnalysis -o myOutputDirectory
</pre>
This will create/save all the results to "myOutputDirectory". Just make sure to change "myAnalysis" to your workspace name! The file we're most interested in is "fit_MLL/results/upperlimit_cls_poi_tree_Asym_CLs_grid_ts3.root.(eps, png, pdf)."


